# Nepali translation for ubuntu-keyboard
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-keyboard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-keyboard\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-18 19:28+0000\n"
"PO-Revision-Date: 2015-11-05 08:33+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Nepali <ne@li.org>\n"
"Language: ne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-04-07 06:31+0000\n"
"X-Generator: Launchpad (build 18343)\n"

#: ../qml/ActionsToolbar.qml:56
msgid "Select All"
msgstr ""

#: ../qml/ActionsToolbar.qml:57
msgid "Redo"
msgstr ""

#: ../qml/ActionsToolbar.qml:58
msgid "Undo"
msgstr ""

#: ../qml/ActionsToolbar.qml:73
msgid "Paste"
msgstr ""

#: ../qml/ActionsToolbar.qml:74
msgid "Copy"
msgstr ""

#: ../qml/ActionsToolbar.qml:75
msgid "Cut"
msgstr ""

#: ../qml/FloatingActions.qml:62
msgid "Done"
msgstr ""

#: ../qml/Keyboard.qml:351
msgid "Swipe to move selection"
msgstr ""

#: ../qml/Keyboard.qml:352
msgid "Swipe to move cursor"
msgstr ""

#: ../qml/Keyboard.qml:352
msgid "Double-tap to enter selection mode"
msgstr ""

#: ../qml/keys/LanguageMenu.qml:106
msgid "Settings"
msgstr "सेटिङ"

#: ../qml/keys/languages.js:19
msgid "Arabic"
msgstr "अरबी"

#: ../qml/keys/languages.js:20
msgid "Azerbaijani"
msgstr "अजरबैजानी"

#: ../qml/keys/languages.js:21
#, fuzzy
#| msgid "Hungarian"
msgid "Bulgarian"
msgstr "हङ्गेरियाली"

#: ../qml/keys/languages.js:22
msgid "Bosnian"
msgstr "बोस्नियाली"

#: ../qml/keys/languages.js:23
msgid "Catalan"
msgstr "क्यातालान"

#: ../qml/keys/languages.js:24
msgid "Czech"
msgstr "चेक"

#: ../qml/keys/languages.js:25
msgid "Danish"
msgstr "डेनिस"

#: ../qml/keys/languages.js:26
msgid "German"
msgstr "जर्मन"

#: ../qml/keys/languages.js:27
msgid "Emoji"
msgstr "भावचिन्ह"

#: ../qml/keys/languages.js:28
msgid "Greek"
msgstr "ग्रिक"

#: ../qml/keys/languages.js:29
msgid "English"
msgstr "अंग्रेजी"

#: ../qml/keys/languages.js:30
msgid "Esperanto"
msgstr ""

#: ../qml/keys/languages.js:31
msgid "Spanish"
msgstr "स्पेनिस"

#: ../qml/keys/languages.js:32
msgid "Persian"
msgstr ""

#: ../qml/keys/languages.js:33
msgid "Finnish"
msgstr "फिनीस"

#: ../qml/keys/languages.js:34
msgid "French"
msgstr "फ्रान्सेली"

#: ../qml/keys/languages.js:35
#, fuzzy
#| msgid "French"
msgid ""
"French\n"
"(Swiss)"
msgstr "फ्रान्सेली"

#: ../qml/keys/languages.js:36
msgid "Scottish Gaelic"
msgstr "स्टकिस गेअलिक"

#: ../qml/keys/languages.js:37
msgid "Hebrew"
msgstr "हिब्रु"

#: ../qml/keys/languages.js:38
msgid "Croatian"
msgstr "क्रोएसियाली"

#: ../qml/keys/languages.js:39
msgid "Hungarian"
msgstr "हङ्गेरियाली"

#: ../qml/keys/languages.js:40
msgid "Icelandic"
msgstr "आइसल्यान्डिक"

#: ../qml/keys/languages.js:41
msgid "Italian"
msgstr "ईटालियन"

#: ../qml/keys/languages.js:42
msgid "Japanese"
msgstr ""

#: ../qml/keys/languages.js:43
msgid "Lithuanian"
msgstr ""

#: ../qml/keys/languages.js:44
msgid "Latvian"
msgstr ""

#: ../qml/keys/languages.js:45
msgid "Korean"
msgstr ""

#: ../qml/keys/languages.js:46
msgid "Dutch"
msgstr "डच"

#: ../qml/keys/languages.js:47
msgid "Norwegian"
msgstr "नर्वेली"

#: ../qml/keys/languages.js:48
msgid "Polish"
msgstr "पोलिस"

#: ../qml/keys/languages.js:49
msgid "Portuguese"
msgstr "पोर्तुगाली"

#: ../qml/keys/languages.js:50
msgid "Romanian"
msgstr "रोमनियाली"

#: ../qml/keys/languages.js:51
msgid "Russian"
msgstr "रसियाली"

#: ../qml/keys/languages.js:52
msgid "Slovenian"
msgstr "स्लोभेनियाली"

#: ../qml/keys/languages.js:53
msgid "Serbian"
msgstr "सर्बियाली"

#: ../qml/keys/languages.js:54
msgid "Swedish"
msgstr "स्विडेनी"

#: ../qml/keys/languages.js:55
msgid "Turkish"
msgstr ""

#: ../qml/keys/languages.js:56
msgid "Ukrainian"
msgstr "युक्रेनियाली"

#: ../qml/keys/languages.js:57
msgid ""
"Chinese\n"
"(Pinyin)"
msgstr ""

#: ../qml/keys/languages.js:58
msgid ""
"Chinese\n"
"(Chewing)"
msgstr ""

#~ msgid "Chinese - Pinyin"
#~ msgstr "चिनियाँ - पिनयिन"
